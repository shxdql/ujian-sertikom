﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance; // init instance

    [SerializeField] Text scoreText, gameOverText; // init score text and game over text in canvas
    [SerializeField] Image gameOverPanel; // init the game over panel in canvas

    [HideInInspector] public int score; // init the score
    [HideInInspector] public bool isDragged; // init boolean for dragging object

    private void Awake()
    {
        Instance = this; // set intance for this script
    }

    private void Start()
    {
        gameOverPanel.gameObject.SetActive(false); // make the game over panel disappear at first
    }

    private void Update()
    {
        scoreText.text = score.ToString(); // set the score text equal to the score

        CheckScore(); // checking score
    }

    void CheckScore()
    {
        if (score < 0) // if the score is less than 0
        {
            StartCoroutine(GameOver()); // start a coroutine to end the game
        }
    }

    IEnumerator GameOver()
    {
        Time.timeScale = 0f; // pause the second in the game
        gameOverPanel.gameObject.SetActive(true); // make the game over panel appear
        gameOverPanel.color = Color.red; // set the game over panel color to red
        gameOverText.text = "lose"; // set the game over text
        yield return new WaitForSeconds(3f); // make a delay for 3 second
        Application.Quit(); // exit the game
    }
}
