﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragAndDropController : MonoBehaviour
{
    private void Start()
    {
        GameManager.Instance.isDragged = false; // set the boolean false
    }

    private void OnMouseDrag()
    {
        transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition); // move the object by the position of the cursor
        GameManager.Instance.isDragged = true; // set the boolean true
    }

    private void OnMouseUp()
    {
        GameManager.Instance.isDragged = false; // set the boolean false
    }
}
