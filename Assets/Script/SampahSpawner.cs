﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampahSpawner : MonoBehaviour
{
    [SerializeField] GameObject[] sampah; // init the prefab of the trash object

    float delay = 1f, times = 0f; // init delay and time for create the trash object

    private void Update()
    {
        times += Time.deltaTime; // add the time by second

        if (times > delay) // if the time is more than the delay
        {
            GameObject child = Instantiate(sampah[Random.Range(0, sampah.Length)], transform.position, transform.rotation); // instantiate/create the trash object
            child.transform.SetParent(transform); // make the trash spawner parent for the trash object
            times = 0f; // reset the time
        }
    }
}
