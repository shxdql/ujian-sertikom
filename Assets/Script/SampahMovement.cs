﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampahMovement : MonoBehaviour
{
    [SerializeField] float speed; // init speed for trash movement

    private void Update()
    {
        if (transform.position.x > -11f) // while the trash x position is less than target x position
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(-11f, transform.position.y), speed * Time.deltaTime); // move the trash within speed multiply second
    }
}
