﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance; // init instance

    public Sound[] sounds; // init the class

    private void Awake()
    {
        Instance = this; // set instance for this script

        foreach (Sound s in sounds) // make copy for target class
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
    }

    public void Play(string name) // play sound by its name
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); // find sound by name
        s.source.Play(); // playing sound
    }
}

[System.Serializable]
public class Sound // class for the audio clip
{
    public string name;

    public AudioClip clip;

    [HideInInspector] public AudioSource source;

    [Range(0f, 1f)] public float volume;
    [Range(.1f, 3f)] public float pitch;
}