﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoring : MonoBehaviour
{
    [SerializeField] Transform sampahSpawner; // init trash spawner

    Transform[] sampahObject; // init trash object

    private void Awake()
    {
        sampahObject = new Transform[99]; // set total array of trash object variable
    }

    private void Update()
    {
        if (sampahSpawner.childCount > 0) // check the trash spawner child (trash object) if it's already created
        {
            for (int i = 0; i < sampahSpawner.childCount; i++) // iteration for getting trash object to take to the its variable
            {
                sampahObject[i] = sampahSpawner.GetChild(i); // set the trash object variable
            }

            for (int i = 0; i < sampahSpawner.childCount; i++) // check the trash spawner child (trash object) if it's already created
            {
                if (!GameManager.Instance.isDragged) // if the boolean is not true
                    CheckTransform(sampahObject[i]); // check the transform range between the trash can and trash object
            }
        }
    }

    void CheckTransform(Transform sampah) // function for checking the transform range between the trash can and trash object
    {
        if (Vector2.Distance(transform.position,sampah.position) < 1f) // check if the transform range between the trash can and trash object is less than target range
        {
            if (gameObject.tag == sampah.gameObject.tag) // check if the trash can tag game object is equal to the trash object tag
            {
                GameManager.Instance.score += 10; // adding score
                SoundManager.Instance.Play("Benar"); // playing sound for the correct choice
            }
            else // if the tag is not equal
            {
                GameManager.Instance.score -= 15; // substracting score
                SoundManager.Instance.Play("Salah"); // playing sound for the wrong choice
            }

            Destroy(sampah.gameObject); // destroy the trash object
        }
    }
}
